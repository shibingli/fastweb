package commons

import (
	goLog "log"
	"sync"

	"gitee.com/shibingli/fastweb/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	Logger *log.Logger

	Env *Environment

	once sync.Once
)

func init() {
	once.Do(loadConfig)
	//loadConfig()
}

func loadConfig() {
	Env = LoadEnvironment()

	var logLevel zapcore.Level

	if Env.DevMode {
		logLevel = zap.DebugLevel
	} else {
		logLevel = zap.InfoLevel
	}

	logger, err := log.NewLogger(logLevel, 128, 7, 30)
	if nil != err {
		goLog.Fatalf("Init logger error. %s\n", err.Error())
	}

	Logger = logger
}

func SetLogger(logger *log.Logger) {
	Logger = logger
}
