package commons

const (
	ServerName         = "fastweb"
	ServerVersion      = "1.0"
	HeaderRequestIDKey = "X-Request-ID"
	JSONContentType    = "application/json;charset=utf-8"
	HTMLContentType    = "text/html;charset=utf-8"
)
